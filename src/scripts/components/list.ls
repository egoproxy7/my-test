{ Column } = require './helpers.ls'

getArr = (arr, filters) ->
  if filters.str
    newArr = []
    for ind in arr
      for key of ind
        if key != 'id'
          if -1 < ind[key].indexOf filters.str
            newArr.push ind
            break
    return newArr.reverse()
  else
    return arr.concat!reverse()

click = (item, evt) ->
  evt.preventDefault()
  return @(item)

module.exports = (props) ->
  <div className="clearfix">
    {
      for ind in getArr(props.items, props.filters)
        <div key={ind.id} className="panel panel-info">
          <div className="panel-heading">
            <h3 className="panel-title">{ind.title + " "}
              <div className="badge" onClick={click.bind(props.click, ind)} >
                <span className={"glyphicon " + props.badge }></span>
              </div>
            </h3>
          </div>
          <div className="panel-body">{ind.descr}</div>
        </div>
    }
  </div>
