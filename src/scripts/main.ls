{ render } = require 'react-dom'

Container = require './components/container.ls'

render do
  <Container />
  document.getElementById('main')
