{ createElement } = React

Fields =
  input: (props) ->
    <div className="form-group">
      <label>{props.text}</label>
      <input name={props.name} type={props.type} className="form-control" placeholder={props.ph} value={props.def} />
    </div>
  textarea: (props) ->
    <div className="form-group">
      <textarea name={props.name} className="form-control" rows="3" placeholder={props.ph} value={props.def}></textarea>
    </div>
  btn: (props) ->
    <button type="submit" className="btn btn-default btn-block">{props.text}</button>

submit = (evt) ->
  evt.preventDefault()
  return @(evt)

module.exports = (props) ->
  <form onSubmit=submit.bind(props.submit) onChange=props.change >
  {
    for ind,key in props.set
      createElement Fields[ind.field], { key: key, ...ind.options, def: props.fields[ind.options.name] }
  }
  {
    if props.err
      <div className="alert alert-danger">{props.err}</div>
  }
  </form>
