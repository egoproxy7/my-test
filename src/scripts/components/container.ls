{ createClass } = React

{ Wrap, Column, Header } = require './helpers.ls'

Form = require './form.ls'
List = require './list.ls'

getForm = ->
  [
    * field: 'input'
      options:
        name: 'title'
        text: 'Добавить заметку:'
        type: 'text'
        ph: 'Введите название...'
    * field: 'textarea'
      options:
        name: 'descr'
        ph: 'Введите сообщение...'
    * field: 'btn'
      options:
        text: 'Сохранить'
  ]

getFilter = ->
  [
    { 
      field: 'input'
      options:
        name: 'str'
        text: 'Найти:'
        type: 'search'
        ph: 'Поиск...' 
    }
  ]

getLocalData = (name, def) ->
  if localStorage.getItem(name)
    return JSON.parse localStorage.getItem(name)
  else
    return def

setLocalData = (name, obj) ->
  localStorage.setItem(name, JSON.stringify(obj))

module.exports = createClass do

  render: ->
    <Wrap>
      <Header />
      <Column>
        <Form set=getForm! change=@_actionChange submit=@_actionSubmit fields=@state.fields err=@state.err />
      </Column>
      <Column>
        <Form set=getFilter! change=@_actionFilter submit=@_actionFilter fields=@state.filters />
        <List items=@state.arr filters=@state.filters badge='glyphicon-pencil' click=@_actionEdit />
      </Column>
    </Wrap>
 
  getInitialState: ->
    return { fields: @_getDefault!, arr: [], err: null, filters: { str: '' }, ...getLocalData('allState', {})} 

  componentDidMount: ->
    watch = ->
      @_action( (prevState, props) ->
        { ...prevState, ...getLocalData('allState', {}) }
      )
    window.addEventListener 'storage', watch.bind(@), false

  componentWillUnmount: ->
    window.removeEventListener 'storage', watch.bind(@), false

  componentDidUpdate: ->
    setLocalData 'allState', @state

  _validation: ->
    if @state.fields.title or @state.fields.descr
      return true
    else
      @setState { err: 'Напишите что-нибудь...' }

  _getDefault: ->
    title: ''
    descr: ''
    id: ''

  _setDefault: (state) ->
    def = 
      title: 'Без названия'
      descr: ''
      id: new Date!getTime()   
    temp = {}
    for ind of state.fields
      if state.fields[ind]
        temp[ind] = state.fields[ind]
      else 
        temp[ind] = def[ind]
    return temp

  _actionSubmit: (evt) ->
    if @_validation!
      @_action( (prevState, props) ->
        submit = (clean, newItem, arr)->
          return { ...prevState, fields: clean, arr: arr.concat([newItem]) }
        if prevState.fields.id
          newArr = []
          for ind, key in prevState.arr
            if ind.id != prevState.fields.id
              newArr.push ind
          return submit(@_getDefault!, prevState.fields, newArr)
        else
          return submit(@_getDefault!, @_setDefault(prevState), prevState.arr)
      )

  _actionChange: (evt) ->
    fields = {}
    fields[evt.target.name] = evt.target.value
    @_action( (prevState, props) ->
      { ...prevState, err: null, fields: { ...prevState.fields, ...fields } }
    )

  _actionFilter: (evt) ->
    target = evt.target.value
    @_action( (prevState, props) ->
      { ...prevState, filters: { str: target } }
    )

  _actionEdit: (item) ->
    @_action( (prevState, props) ->
      { ...prevState, err: null, fields: { ...item } }
    )

  _action: (callback) ->
    @setState( (prevState, props) ->
      return callback.call(@, prevState, props)
    )
