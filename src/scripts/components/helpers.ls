module.exports =
  Wrap: (props) ->
    <div className="container">
      <div className="row">{props.children}</div>
    </div>

  Column: (props) ->
    <div className="col-xs-12 col-sm-12 col-md-6">{props.children}</div>

  Header: (props) ->
    <div className="page-header">
      <h1>Заметки<small>, добавить:</small></h1>
    </div>