var path = require('path');
var webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

// var devFlagPlugin = new webpack.DefinePlugin({
//   __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'false'))
// });

module.exports = {
  context: path.join(__dirname, 'src'),
  progress: true,
  entry: [
    'webpack/hot/only-dev-server',
    './scripts/main.ls',
    './styles/main.less'
  ],
  output: {
    path: path.join(__dirname, 'assets'),
    filename: 'bundle.js',
    pathinfo: true
  },
  devtool: 'eval',
  module: {
    loaders: [
      {
        test: /\.ls$/,
        loader: "livescript-loader!cjsx"
      },
      { 
      //   test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      //   loader: "url-loader?limit=10000&minetype=application/font-woff" },
      // { 
      //   test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
      //   loader: "file-loader" 
      // }, {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract("css!less")
      }, 
    ]
  },
  resolve: {
    modulesDirectories: ['node_modules', 'scripts'],
    extensions: [""].concat(['.js', '.ls', '.cjsx'])
  },
  plugins: [
    new ExtractTextPlugin('bundle.css'),
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'React': 'react',
      'ReactDOM': 'react-dom'
      // 'classNames': 'classnames',
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    // devFlagPlugin,
    new webpack.optimize.DedupePlugin()

    // , new webpack.DefinePlugin({
    //   'process.env': {
    //     'NODE_ENV': '"production"'
    //   }
    // })
    // , new webpack.optimize.UglifyJsPlugin({
    //   compress: { warnings: false },
    //   comments: false,
    //   sourceMap: false,
    //   mangle: true,
    //   minimize: true
    // })
  ]
}
